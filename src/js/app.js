import CheckCookies from "@components/cookie-message";
import header from "@components/header";
import { sliderCommon } from "@components/sliders/common";
import { sliderActions } from "@components/sliders/actions";
import anchors from "@components/scroll";
import { popupCallback, popupAdvantage, popupVideo } from "@components/popup";

import "@components/quiz";
import "@components/forms";
import "@components/apartments";
import { progress } from "@components/progress";
import { simpleTab } from "@components/tab-group";
import { showMoreDocuments } from "@components/showMore";
import map from "@components/map";
import "@components/calculator";

document.addEventListener("DOMContentLoaded", () => {
    new CheckCookies();
    header.init();
    sliderCommon.init();
    sliderActions.init();
    progress.init();
    anchors.init();
    showMoreDocuments.init();
    simpleTab.init();

    map.render();

    popupCallback.init();
    popupAdvantage.init();
    popupVideo.init();
});
