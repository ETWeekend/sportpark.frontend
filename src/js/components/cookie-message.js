class CheckCookies {
    constructor() {
        this.cookieDate = localStorage.getItem("cookieDate");
        this.cookieNotification = document.querySelector("[data-cookie-message]");
        this.cookieBtn = this.cookieNotification.querySelector("[data-cookie-btn]");
        this.showMessage();
        this.events();
    }

    events() {
        this.cookieBtn.addEventListener("click", this.acceptCookies.bind(this), true);
    }

    showMessage() {
        if (!this.cookieDate || +this.cookieDate + 31536000000 < Date.now()) {
            this.cookieNotification.classList.add("active");
        }
    }

    acceptCookies() {
        localStorage.setItem("cookieDate", Date.now());
        this.cookieNotification.classList.remove("active");
    }
}

export default CheckCookies;
