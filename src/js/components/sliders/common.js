import Slider from "@components/sliders/Slider";
import { breakpoint } from "@utils/devices";

export const sliderCommon = new Slider({
    wrap: "[data-slider-common-wrap]",
    slider: "[data-slider-common]",
    prev: "[data-nav-arrow-prev]",
    next: "[data-nav-arrow-next]",
    fractionPagination: true,
    options: {
        slidesPerView: 1,
        spaceBetween: 0, // задается стилями в rem
        speed: 700,
        roundLengths: true,
        breakpoints: {
            [breakpoint.md]: {
                slidesPerView: 2,
            },
            [breakpoint.lg]: {
                slidesPerView: 3,
            },
        },
    },
});
