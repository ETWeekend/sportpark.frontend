import Slider from "@components/sliders/Slider";

export const sliderActions = new Slider({
    wrap: "[data-slider-actions-wrap]",
    slider: "[data-slider-actions]",
    prev: "[data-nav-arrow-prev]",
    next: "[data-nav-arrow-next]",
    fractionPagination: true,
    options: {
        slidesPerView: 1,
        spaceBetween: 24,
        speed: 700,
        roundLengths: true,
        autoHeight: true,
    },
});
