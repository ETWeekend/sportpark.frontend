import noUiSlider from "nouislider";
import wNumb from "wnumb";
import API_URL from "@utils/API_URL";

class Calculator {
    constructor(options) {
        this.$options = options;

        this.$priceRange = document.querySelector(this.$options.inputs.priceRange);
        this.$priceField = document.querySelector(this.$options.inputs.priceField);
        this.$paymentRange = document.querySelector(this.$options.inputs.paymentRange);
        this.$paymentField = document.querySelector(this.$options.inputs.paymentField);
        this.$termRange = document.querySelector(this.$options.inputs.termRange);
        this.$termField = document.querySelector(this.$options.inputs.termField);

        window.calcResult = {};

        if ((this.$priceRange && this.$paymentRange && this.$termRange) !== null) {
            this.getData();
        }
    }

    getData() {
        const url = API_URL + "/api/?controller=main&method=calculator";
        fetch(url)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw Error("Error request");
                }
            })
            .then((result) => {
                this.data = result.data;
                this.init();
            });
    }

    init() {
        const minTime = 1;
        const firstBank = this.data.CALC[0];
        const calcPrices = this.data.PRICE;
        let calcPercent = firstBank.RATE;

        //<editor-fold desc="Минимальный процент для первоначального взноса">
        const getRangeMin = (rate) => {
            let rangeMin = 40;
            for (let value of Object.entries(rate)) {
                this.#focusoutIR(value[1] + " %");
                if (value[0] < rangeMin) {
                    rangeMin = value[0];
                }
            }
            return rangeMin;
        };
        const rangeMin = getRangeMin(calcPercent);
        //</editor-fold>

        //<editor-fold desc="Стоимость квартиры">
        noUiSlider.create(this.$priceRange, {
            start: +calcPrices.min,
            connect: "lower",
            range: {
                min: +calcPrices.min,
                max: +calcPrices.max,
            },
            format: wNumb({
                decimals: 0,
                thousand: " ",
            }),
        });
        this.$priceRange.noUiSlider.on("update", (values, handle) => {
            this.$priceField.value = values[handle];
            this.#showPurchase(values[handle]);
        });
        this.$priceField.addEventListener("change", (event) => {
            this.$priceRange.noUiSlider.set(event.target.value);
        });
        //</editor-fold>

        //<editor-fold desc="Первоначальный взнос">
        noUiSlider.create(this.$paymentRange, {
            start: +rangeMin,
            connect: "lower",
            range: {
                min: +rangeMin,
                max: 70,
            },
            format: wNumb({
                decimals: 0,
                thousand: " ",
            }),
        });
        this.$paymentRange.noUiSlider.on("update", (values, handle) => {
            this.$paymentField.value = values[handle];
            const value = parseInt(values[handle], 10);
            let offset = Infinity;
            let key = 0;

            for (const dirtyKey of Object.keys(calcPercent)) {
                const currentKey = parseInt(dirtyKey, 10);
                if (currentKey > value) {
                    continue;
                }
                const currentOffset = value - currentKey;
                if (currentOffset < offset) {
                    key = currentKey;
                    offset = currentOffset;
                }
            }

            const percent = calcPercent[key];

            this.#focusoutIR(percent + " %");
            this.#changedDP(values[handle]);

            window.calcResult.percent = percent; // текущая ставка
        });
        this.$paymentField.addEventListener("change", (event) => {
            let newVal = this.$paymentField.value;
            this.#inputChangeDP(newVal);
        });
        //</editor-fold>

        //<editor-fold desc="Срок выплат">
        noUiSlider.create(this.$termRange, {
            start: "20",
            connect: "lower",
            range: {
                min: minTime,
                max: firstBank.TIME,
            },
            format: wNumb({
                decimals: 0,
                thousand: " ",
            }),
        });
        this.$termRange.noUiSlider.on("update", (values, handle) => {
            let value = values[handle];
            let text;
            let count = value % 100;
            if (count >= 5 && count <= 20) {
                text = "лет";
            } else {
                count = count % 10;
                if (count === 1) {
                    text = "год";
                } else if (count >= 2 && count <= 4) {
                    text = "года";
                } else {
                    text = "лет";
                }
            }

            this.$termField.value = value + " " + text;
            this.verify();
        });
        this.$termField.addEventListener("change", (event) => {
            this.$termRange.noUiSlider.set(event.target.value);
        });
        //</editor-fold>
    }

    #numberWithCommas(x) {
        return new Intl.NumberFormat("ru-RU").format(x);
    }

    #showPurchase(newValue) {
        let pp = null;
        if (typeof newValue === "string") {
            pp = newValue.replace(/ +/g, "");
        } else {
            pp = newValue;
        }
        this.$priceField.value = this.#numberWithCommas(pp) + " ₽";
        const dp = document.getElementById("range2").value;

        this.$paymentField.value = this.#numberWithCommas(((parseInt(dp, 10) / 100) * pp).toFixed(0)) + " ₽"; //no decimals
        this.verify();
    }

    #showDownPay(newValue) {
        let pp = null;
        const dp = newValue;
        pp = parseInt(this.$priceField.value.toString().replace(/[^a-zA-Z 0-9]+/g, ""), 10); //remove all special chars
        document.getElementById("range2").value = dp + " %";
        this.$paymentField.value = this.#numberWithCommas(((parseInt(dp, 10) / 100) * pp).toFixed(0)) + " ₽";
        this.$priceField.value = this.#numberWithCommas(pp) + " ₽";
        this.verify();
    }

    #changedPP(newValue) {
        let pp = parseInt(newValue.toString().replace(/[^a-zA-Z 0-9]+/g, ""), 10);
        if (isNaN(pp)) {
            pp = "0";
        }
        this.$priceField.value = this.#numberWithCommas(pp) + " ₽";
        var dp = document.getElementById("range2").value;
        this.$paymentField.value = this.#numberWithCommas(((parseInt(dp, 10) / 100) * pp).toFixed(0)) + " ₽"; //no decimals
        this.verify();
    }

    #changedDP(newValue) {
        const dp = newValue;
        const pp = parseInt(this.$priceField.value.toString().replace(/[^a-zA-Z 0-9]+/g, ""), 10);

        document.getElementById("range2").value = dp;
        this.$paymentField.value = this.#numberWithCommas(((parseInt(dp, 10) / 100) * pp).toFixed(0)) + " ₽";
        this.verify();
    }

    #inputChangeDP(newValue) {
        const dp = parseInt(newValue.replace(/\s+/g, "", 10));
        const range1ValueNew = parseInt(this.$priceRange.noUiSlider.get().replace(/\s/g, ""));
        const calc = (dp * 100) / range1ValueNew;
        this.$priceRange.noUiSlider.set(range1ValueNew);
        this.#showPurchase(range1ValueNew);

        document.getElementById("range2").value = calc;
        this.$paymentRange.noUiSlider.set(calc);
        this.verify();
    }

    #focusoutDP(newValue) {
        let dp = newValue.replace(/%/gi, "");
        if (isNaN(dp)) {
            dp = "0";
            this.$paymentField.value = "0";
        }
        document.getElementById("range2").value = dp + " %";
        this.verify();
    }

    #changedDPV(newValue) {
        var dpv = parseInt(newValue.toString().replace(/[^a-zA-Z 0-9]+/g, ""), 10);
        var pp = parseInt(this.$priceField.value.toString().replace(/[^a-zA-Z 0-9]+/g, ""), 10);
        if (isNaN(dpv)) {
            dpv = "0";
        }
        document.getElementById("range2").value = this.#numberWithCommas(((dpv / pp) * 100).toFixed(0)) + " %";
        this.$paymentField.value = this.#numberWithCommas(dpv);
        this.verify();
    }

    #changedIR(newValue) {
        var ir = newValue;
        document.getElementById("interest").value = ir + " %";
        this.verify();
    }

    #focusoutIR(newValue) {
        var ir = newValue.replace(/%/gi, "");
        document.getElementById("interest").value = ir + " %";

        // document.querySelectorAll(".calc__form-title span").forEach((item) => {
        //     item.innerHTML = ir + "%";
        // });
        this.verify();
    }

    calculate() {
        var mt = parseInt(
            this.$termField.value
                .toString()
                .replace(/[^a-zA-Z 0-9]+/g, "")
                .replace(/ +/g, ""),
            10
        );
        var pp = parseInt(this.$priceField.value.toString().replace(/[^a-zA-Z 0-9]+/g, ""), 10);
        var dpv = parseInt(this.$paymentField.value.toString().replace(/[^a-zA-Z 0-9]+/g, ""), 10);
        var ir = parseFloat(document.getElementById("interest").value, 10);
    }

    show() {
        var mt = parseInt(
            this.$termField.value
                .toString()
                .replace(/[^a-zA-Z 0-9]+/g, "")
                .replace(/ +/g, ""),
            10
        );
        var pp = parseInt(this.$priceField.value.toString().replace(/[^a-zA-Z 0-9]+/g, ""), 10);
        var dpv = parseInt(this.$paymentField.value.toString().replace(/[^a-zA-Z 0-9]+/g, ""), 10);

        var ir = parseFloat(document.getElementById("interest").value, 10);
        var princ = pp - dpv;
        var intRate = ir / 100 / 12;
        var months = mt * 12;

        const resultTotal = this.#numberWithCommas(
            ((((princ * intRate) / (1 - Math.pow(1 + intRate, -1 * months))) * 100) / 100).toFixed(0)
        );

        window.calcResult.price = pp; // стоимость квартиры
        window.calcResult.initialFee = dpv; // первоначальный взнос
        window.calcResult.monthly = resultTotal; // ежемесячный платеж
    }

    verify() {
        this.show();
    }
}

new Calculator({
    inputs: {
        priceRange: "#calcPrice",
        priceField: "#calcPriceField",
        paymentRange: "#calcInitial",
        paymentField: "#calcInitialField",
        termRange: "#calcPayment",
        termField: "#calcPaymentField",
    },
});
