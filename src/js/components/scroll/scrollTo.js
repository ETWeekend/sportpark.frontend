const scrollTo = (element, padding = 0, duration = 1250) => {
    let target;
    if (typeof element === "string") {
        target = document.querySelector(element);
    } else {
        target = element;
    }

    const startY = window.pageYOffset;
    const elementY = window.pageYOffset + target.getBoundingClientRect().top;

    // If element is close to page's bottom then window will scroll only to some position above the element.
    const targetY =
        document.body.scrollHeight - elementY < window.innerHeight
            ? document.body.scrollHeight - window.innerHeight
            : elementY;

    const diff = targetY - startY;
    const endY = diff - padding;
    let start;

    const easeInOutCubic = (t) => (t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1);

    const step = (timestamp) => {
        if (!start) {
            start = timestamp;
        }
        // Elapsed milliseconds since start of scrolling.
        const time = timestamp - start;
        // Get percent of completion in range [0, 1].
        let percent = Math.min(time / duration, 1);
        // Apply the easing.
        // It can cause bad-looking slow frames in browser performance tool, so be careful.
        percent = easeInOutCubic(percent);

        window.scrollTo(0, startY + endY * percent);

        // Proceed with animation as long as we wanted it to.
        if (time < duration) {
            window.requestAnimationFrame(step);
        }
    };

    window.requestAnimationFrame(step);
};

export default scrollTo;
