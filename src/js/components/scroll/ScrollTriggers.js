import scrollTo from "@components/scroll/scrollTo";

class ScrollTriggers {
    constructor(props) {
        this.trigger = props.trigger;
        this.padding = props.padding;
        this.duration = props.duration || 1000;
    }

    init() {
        const triggers = document.querySelectorAll(this.trigger);

        triggers.forEach((trigger) => {
            trigger.addEventListener("click", (event) => {
                event.preventDefault();

                let padding = 0;
                if (this.padding !== undefined) {
                    padding = this.padding;
                } else if (document.querySelector(".header-bar")) {
                    padding = document.querySelector(".header-bar").clientHeight + 24;
                }

                const href = trigger.getAttribute("href");
                if (!href) console.error("Отсутствует href для", this.trigger);

                let target;
                if (href === "#" || href === "") {
                    target = document.body;
                    padding = 0;
                } else {
                    target = document.querySelector(href);
                }
                if (!target) console.error("Отсутствует target для", this.trigger);

                scrollTo(target, padding);
            });
        });
    }
}

export default ScrollTriggers;
