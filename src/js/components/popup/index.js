import Popup from "@components/popup/Popup";

const popupCallback = new Popup({
    popup: "[data-popup-callback]",
    trigger: "[data-popup-callback-trigger]",
    lockScroll: true,
    form: true,
    onDocument: true,
    onOpen: (event) => {
        const popup = event.detail.popup;
        const trigger = event.detail.trigger;

        const formName = trigger.getAttribute("data-popup-callback-trigger");
        if (formName) {
            popup.querySelector("[data-popup-formname]").value = formName;
            popup.querySelector("[data-popup-heading]").innerText = formName;
        }

        const inputs = trigger.getAttribute("data-popup-inputs");
        if (inputs) {
            const inputsBox = popup.querySelector("[data-form-inputs-box]");
            if (inputsBox) {
                JSON.parse(inputs).forEach((input) => {
                    for (let key in input) {
                        if (input.hasOwnProperty(key)) {
                            inputsBox.insertAdjacentHTML(
                                "beforeend",
                                `<input type="hidden" name="${key}" value="${input[key]}">`
                            );
                        }
                    }
                });
            }
        }

        const isCalcBtn = trigger.hasAttribute("data-calc-btn");
        if (isCalcBtn) {
            const inputsBox = popup.querySelector("[data-form-inputs-box]");
            if (inputsBox) {
                for (let key in window.calcResult) {
                    if (window.calcResult.hasOwnProperty(key)) {
                        inputsBox.insertAdjacentHTML(
                            "beforeend",
                            `<input type="hidden" name="${key}" value="${window.calcResult[key]}">`
                        );
                    }
                }
            }
        }
    },
    onClose: (event) => {
        const popup = event.detail.popup;
        const nameInput = popup.querySelector("[data-popup-formname]");
        const heading = popup.querySelector("[data-popup-heading]");
        if (nameInput) {
            const value = nameInput.getAttribute("data-popup-formname");
            if (value) {
                nameInput.value = value;
                nameInput.innerText = value;
            }

            if (heading) heading.innerText = value;
        }

        const inputsBox = popup.querySelector("[data-form-inputs-box]");
        if (inputsBox) inputsBox.innerHTML = "";
    },
});

const popupVideo = new Popup({
    popup: "[data-popup-video]",
    trigger: "[data-popup-video-trigger]",
    lockScroll: true,
    onDocument: true,
    onOpen: (event) => {
        const popup = event.detail.popup;
        const trigger = event.detail.trigger;

        const videoSrc = trigger.getAttribute("data-youtube-src");
        if (videoSrc) popup.querySelector("iframe").setAttribute("src", videoSrc);
    },
    onClose: (event) => {
        const popup = event.detail.popup;
        popup.querySelector("iframe").setAttribute("src", "");
    },
});

const popupThanks = new Popup({
    popup: "[data-popup-thanks]",
    trigger: "[data-popup-thanks-trigger]",
    lockScroll: true,
    onClose: () => {
        document.dispatchEvent(new CustomEvent("closeAllPopups"));
    },
});
const popupError = new Popup({
    popup: "[data-popup-error]",
    trigger: "[data-popup-error-trigger]",
    lockScroll: true,
    onClose: () => {
        document.dispatchEvent(new CustomEvent("closeAllPopups"));
    },
});

const popupAdvantage = new Popup({
    popup: "[data-popup-advantage]",
    trigger: "[data-popup-advantage-trigger]",
    lockScroll: true,
});

export { popupCallback, popupThanks, popupError, popupAdvantage, popupVideo };
