// костыльная тема, которая работает только вертикально (количество элементов)
// надо написать что-нибудь поумнее (:

import { isMobile } from "@utils/devices";
import { throttle } from "@utils/helpers";

class ShowMore {
    constructor(props) {
        this.wrap = props.wrap;
        this.container = props.container;
        this.element = props.element;
        this.btn = props.btn;
        this.amount = props.amount;
        this.onlyMob = props.onlyMob;
        this.duration = 300;
    }

    init() {
        const wraps = document.querySelectorAll(this.wrap);
        wraps.forEach((wrap) => {
            const container = wrap.querySelector(this.container);
            container.expanded = true;
            container.style.transitionProperty = "height";
            container.style.transitionDuration = `${this.duration}ms`;
            container.style.overflow = "hidden";

            const elements = container.querySelectorAll(this.element);
            if (!elements.length) return;

            const btn = wrap.querySelector(this.btn);

            btn.addEventListener("click", () => {
                if (container.expanded) {
                    this.rollUp(container, elements, btn);
                } else {
                    this.expand(container, elements, btn);
                }
            });

            window.addEventListener("resize", throttle(this.onResize.bind(this, container, elements, btn), 15));

            if (this.onlyMob && isMobile()) {
                btn.dispatchEvent(new CustomEvent("click"));
            } else if (this.onlyMob === undefined) {
                btn.dispatchEvent(new CustomEvent("click"));
            }
        });
    }

    onResize(container, elements, btn) {
        if (this.onlyMob) {
            if (!container.expanded) {
                if (isMobile()) {
                    const elementsHeight = this.getHeight(elements);
                    container.style.height = `${elementsHeight}px`;
                } else {
                    btn.dispatchEvent(new CustomEvent("click"));
                }
            }
        } else if (!container.expanded) {
            const elementsHeight = this.getHeight(elements);
            container.style.height = `${elementsHeight}px`;
        }
    }

    expand(container, elements, btn) {
        btn.classList.add("reverse-text");

        const elementsHeight = this.getHeight(elements);
        container.style.removeProperty("transition-duration");
        container.style.removeProperty("transition-property");
        container.style.removeProperty("height");
        const sourceHeight = container.offsetHeight;

        container.style.height = `${elementsHeight}px`;
        container.offsetHeight;
        container.style.transitionProperty = "height";
        container.style.transitionDuration = `${this.duration}ms`;
        container.style.height = `${sourceHeight}px`;

        setTimeout(() => {
            container.style.removeProperty("height");
        }, this.duration);

        container.expanded = true;
    }

    rollUp(container, elements, btn) {
        btn.classList.remove("reverse-text");

        const elementsHeight = this.getHeight(elements);
        container.style.height = `${container.offsetHeight}px`;
        container.offsetHeight;
        container.style.height = `${elementsHeight}px`;

        container.expanded = false;
    }

    getHeight(elements) {
        let height = 0;
        for (let i = 0; i < this.amount; i++) {
            height += +elements[i].offsetHeight;

            if (i < this.amount - 1) {
                height += +window.getComputedStyle(elements[i]).marginBottom.replace("px", "");
            }
        }
        return height;
    }
}

const showMoreDocuments = new ShowMore({
    wrap: "[data-more-documents-wrap]",
    container: "[data-more-documents-container]",
    element: "[data-more-document]",
    btn: "[data-more-documents-btn]",
    amount: 3,
    onlyMob: true,
});

export { showMoreDocuments };
