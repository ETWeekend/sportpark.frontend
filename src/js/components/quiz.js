import Quiz from "@southmedia/quiz";

const quizWrap = document.querySelector("[data-quiz-wrap]");
const quiz = quizWrap.querySelector("[data-quiz-inline]");
const quizSteps = quiz.querySelectorAll("[data-quiz-question]");
const quizBullets = quiz.querySelectorAll("[data-quiz-bullet]");
const quizImages = quizWrap.querySelectorAll("[data-quiz-image]");

if (quizSteps.length !== quizBullets.length) {
    console.error(
        `[Quiz]: Количество точек пагинации не соответствует количеству вопросов`,
        quizSteps.length,
        quizBullets.length
    );
}

quizBullets[0].classList.add("active");
quizImages[0].classList.add("active");

new Quiz(quiz, {
    defaultTemplate: false,
    onChange: (oldIndex, newIndex, length, selector) => {
        quizBullets.forEach((bullet, index) => {
            if (index <= newIndex) {
                bullet.classList.add("active");
            } else {
                bullet.classList.remove("active");
            }
        });

        quizImages.forEach((image, index) => {
            if (index === newIndex) {
                image.classList.add("active");
            } else {
                image.classList.remove("active");
            }
        });
    },
    onComplete: (selector, arr) => {
        const processing = quizWrap.querySelector("[data-quiz-processing]");
        const final = quizWrap.querySelector("[data-quiz-final]");
        processing.classList.add("active");

        quizImages.forEach((image, index) => {
            if (index === quizImages.length - 1) {
                image.classList.add("active"); // активировать последнее изображение
            } else {
                image.classList.remove("active");
            }
        });

        setTimeout(() => {
            processing.classList.remove("active");
            final.classList.add("active");
            const form = final.querySelector("form");
            form.extraFormData = arr;
        }, 3000);
    },
    onError: (container, err) => {},
});
