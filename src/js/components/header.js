import { debounce, throttle } from "@utils/helpers";

class Header {
    constructor() {
        this.header = document.querySelector("[data-header]");
        this.body = document.body;
        this.duration = 300;
        this.isMenuOpened = false;
    }

    init() {
        if (!this.header) return;

        this.menuSwitch = document.querySelector("[data-menu-switch]");
        this.navMenu = document.querySelector("[data-nav-menu]");
        if (this.menuSwitch && this.navMenu) {
            this.menuActions();
            this.scrollOperations();
        }
    }

    menuActions() {
        this.menuSwitch.addEventListener("click", () => {
            if (this.isMenuOpened) {
                this.closeMenu();
            } else {
                this.openMenu();
            }
        });

        document.addEventListener("keyup", (event) => {
            if (event.key === "Escape" || event.key === "Esc") {
                this.closeMenu();
            }
        });

        const anchors = this.header.querySelectorAll("[data-anchor]");
        if (anchors.length) {
            anchors.forEach((anchor) => {
                anchor.addEventListener("click", () => {
                    this.closeMenu();
                });
            });
        }
    }

    openMenu() {
        this.navMenu.style.display = "block";
        const height = this.navMenu.offsetHeight;
        this.navMenu.style.overflow = "hidden";
        this.navMenu.style.height = "0";
        this.navMenu.style.paddingTop = 0;
        this.navMenu.style.paddingBottom = 0;
        this.navMenu.offsetHeight;
        this.navMenu.style.transitionProperty = "height";
        this.navMenu.style.transitionDuration = `${this.duration}ms`;
        this.navMenu.style.height = `${height}px`;
        this.navMenu.style.removeProperty("padding-top");
        this.navMenu.style.removeProperty("padding-bottom");

        setTimeout(() => {
            this.navMenu.style.removeProperty("height");
            this.navMenu.style.removeProperty("overflow");
            this.navMenu.style.removeProperty("transition-duration");
            this.navMenu.style.removeProperty("transition-property");
        }, this.duration);

        this.body.classList.add("menu-opened");
        this.isMenuOpened = true;
    }

    closeMenu() {
        this.navMenu.style.transitionProperty = "height";
        this.navMenu.style.transitionDuration = `${this.duration}ms`;
        this.navMenu.style.height = `${this.navMenu.offsetHeight}px`;
        this.navMenu.offsetHeight;
        this.navMenu.style.overflow = "hidden";
        this.navMenu.style.height = "0";
        this.navMenu.style.paddingTop = 0;
        this.navMenu.style.paddingBottom = 0;

        setTimeout(() => {
            this.navMenu.style.removeProperty("display");
            this.navMenu.style.removeProperty("padding-top");
            this.navMenu.style.removeProperty("padding-bottom");
            this.navMenu.style.removeProperty("height");
            this.navMenu.style.removeProperty("overflow");
            this.navMenu.style.removeProperty("transition-duration");
            this.navMenu.style.removeProperty("transition-property");
        }, this.duration);

        this.body.classList.remove("menu-opened");
        this.isMenuOpened = false;
    }

    scrollOperations() {
        const scrollHandler = () => {
            this.scrollFromZero();
        };
        scrollHandler();

        // const resizeHandler = () => {};

        window.addEventListener("scroll", throttle(scrollHandler, 15));
        // window.addEventListener("resize", debounce(resizeHandler, 100));
    }

    scrollFromZero() {
        if (this.isMenuOpened) this.closeMenu();

        if (window.pageYOffset > 4) {
            this.body.classList.add("scrolled-from-zero");
        } else {
            this.body.classList.remove("scrolled-from-zero");
        }
    }
}

export default new Header();
