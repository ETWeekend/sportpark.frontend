import { Validate } from "@components/forms/Validate";
import { popupThanks, popupError } from "@components/popup";

popupThanks.init();
popupError.init();

const options = {
    selector: "[data-form-validate]",
    requiredClasses: ".required",
    errorClass: "sm-validate__error",
    errorMessage: "data-field-error-message",
    errorSelector: "[data-field-error]",
    phoneMask: {
        mask: "+{7}(000)000-00-00",
    },
    isPhoneInput: "input[type='tel']",
    parent: "[data-field]",
    lang: "ru-RU",
    send: (el, data) => {
        const url = el.getAttribute("action");
        if (!url) console.error("[FORM]: отсутствует action для", el);

        fetch(url, {
            method: "POST",
            body: data,
        })
            .then((resolve) => {
                if (!resolve.ok) return popupError.openPopup(document.querySelector("[data-popup-error]"));
                popupThanks.openPopup(document.querySelector("[data-popup-thanks]"));
            })
            .catch((error) => {
                popupError.openPopup(document.querySelector("[data-popup-error]"));
                console.error("[FORM]: ", error.message);
            });
    },
    success: (el) => {
        el.dispatchEvent(new CustomEvent("formCompleted"));
    },
    error: (el) => {
        console.error(el, " error");
        el.dispatchEvent(new CustomEvent("formCompleted"));
    },
};

window.forms = new Validate(options);
