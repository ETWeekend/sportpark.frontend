import Swiper from "swiper";
import API_URL from "@utils/API_URL";

class Apartments {
    constructor() {
        this.apartments = document.querySelector("[data-apartments]");
        if (!this.apartments) return;

        this.slider = this.apartments.querySelector("[data-slider-apartments]");
        this.prev = this.apartments.querySelector("[data-nav-arrow-prev]");
        this.next = this.apartments.querySelector("[data-nav-arrow-next]");
        this.bullets = this.apartments.querySelector("[data-slider-pagination]");

        this.renderTabs();
    }

    getData(type) {
        const url = API_URL + `/api/?controller=main&method=get_apartments_layouts&type=${type}`;
        return fetch(url)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw Error("Error request");
                }
            })
            .then((result) => {
                return result.data;
            });
    }

    renderTabs() {
        const tabs = this.apartments.querySelectorAll("[data-apartments-tab]");
        if (!tabs.length) return;

        tabs.forEach((tab) => {
            tab.addEventListener("click", () => {
                if (tab.classList.contains("active")) return;

                tabs.forEach((item) => {
                    item.classList.remove("active");
                });
                tab.classList.add("active");

                const currentType = tab.getAttribute("data-apartments-tab");
                this.renderCards(currentType);
            });
        });

        const observer = new IntersectionObserver(
            (entries, observer) => {
                entries.forEach((entry) => {
                    if (entry.isIntersecting) {
                        observer.unobserve(entry.target);
                        tabs[0].dispatchEvent(new CustomEvent("click"));
                    }
                });
            },
            {
                rootMargin: "100px 0px",
            }
        );
        observer.observe(this.apartments);
    }

    renderCards(type) {
        new Promise((resolve) => {
            resolve(this.getData(type));
        }).then((data) => {
            const swiperWrapper = this.slider.querySelector(".swiper-wrapper");
            swiperWrapper.innerHTML = "";
            data.forEach((card) => swiperWrapper.insertAdjacentHTML("beforeend", this.cardTemplate(card)));

            if (this.slider.swiper) {
                this.slider.swiper.destroy();
            }

            new Swiper(this.slider, {
                roundLengths: true,
                slidesPerView: "auto",
                spaceBetween: 0, // задано на стилях в rem
                speed: 700,
                loop: false,
                navigation: {
                    prevEl: this.prev,
                    nextEl: this.next,
                    disabledClass: "disabled",
                },
            });
        });
    }

    cardTemplate(card) {
        let dataForPopup = `{"apartment_id":"${card.ID}"},{"apartment_name":"${card.NAME}"}`;
        if (card.SQUARE) {
            dataForPopup += `,{"apartment_square_total":"${card.SQUARE}"}`;
        }

        return `<div class="swiper-slide">
            <div class="card-apartment" data-popup-callback-trigger="Узнать стоимость"
                data-popup-inputs='[${dataForPopup}]'>
                <div class="card-apartment__image">
                    <img src="${API_URL + card.IMAGE}" alt="" loading="lazy">
                </div>
                <div class="card-apartment__heading h3">${card.NAME}</div>
                ${
                    card.SQUARE
                        ? `<div class="card-apartment__desc text-default">Общая площадь ${card.SQUARE} м²</div>`
                        : ""
                }
                <div class="card-apartment__btn">
                    <button type="button" class="btn btn--default btn--primary">
                        <span class="btn__text">Узнать стоимость</span>
                    </button>
                </div>
            </div>
        </div>`;
    }
}

new Apartments();
