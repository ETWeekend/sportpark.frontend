import API_URL from "@utils/API_URL";
import "lightgallery.js";
import "lg-video.js";

class Progress {
    constructor(props) {
        this.trigger = props.trigger;
    }

    init() {
        document.addEventListener("click", (event) => {
            const trigger = event.target.closest(this.trigger);
            if (!trigger) return;

            const month = trigger.getAttribute("data-progress-month");
            if (!month) console.error(`[progress]: не установлен data-progress-month`);
            const year = trigger.getAttribute("data-progress-year");
            if (!year) console.error(`[progress]: не установлен data-progress-year`);

            this.getData(month, year).then((data) => this.openGallery(trigger, data));
        });
    }

    getData(month, year) {
        const url = API_URL + `/api/?controller=main&method=get_progress&month=${month}&year=${year}`;
        return fetch(url)
            .then((response) => {
                if (response.ok) return response.json();
                else throw Error("Error request");
            })
            .then((result) => result.data);
    }

    openGallery(card, data) {
        const galleryItems = [];

        data.PHOTOS.forEach((item) => {
            galleryItems.push({
                src: API_URL + item,
            });
        });

        // для видео необходим доп. плагин https://github.com/sachinchoolur/lg-video.js
        if (data.VIDEO) {
            galleryItems.push({
                src: data.VIDEO,
            });
        }

        lightGallery(card, {
            dynamic: true,
            dynamicEl: galleryItems,
        });
    }
}

const progress = new Progress({
    trigger: "[data-progress-gallery]",
});

export { progress };
