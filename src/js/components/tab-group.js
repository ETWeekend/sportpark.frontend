class TabGroup {
    constructor(props) {
        this.group = props.group;
        this.trigger = "data-tab-trigger";
        this.content = "data-tab-content";
        this.activeClass = "active";
    }

    init() {
        const groups = document.querySelectorAll(this.group);
        if (!groups.length) return;

        groups.forEach((group) => {
            const triggers = group.querySelectorAll(`[${this.trigger}]`);
            const contents = group.querySelectorAll(`[${this.content}]`);

            triggers.forEach((item) => {
                const currentTrigger = item;
                const index = currentTrigger.getAttribute(this.trigger);
                const contentBelong = group.querySelector(`[${this.content}` + "=" + `"${index}"]`);

                currentTrigger.addEventListener("click", () => {
                    if (currentTrigger.classList.contains(this.activeClass)) return;

                    triggers.forEach((trigger) => {
                        if (trigger !== currentTrigger) {
                            this.closeTrigger(trigger);
                        }
                    });
                    contents.forEach((content) => {
                        if (content !== contentBelong) {
                            this.closeContent(content);
                        }
                    });

                    this.openTrigger(currentTrigger);
                    this.openContent(contentBelong);
                });
            });
        });
    }

    openTrigger(trigger) {
        trigger.classList.add(this.activeClass);
    }

    closeTrigger(trigger) {
        trigger.classList.remove(this.activeClass);
    }

    openContent(content) {
        content.classList.add(this.activeClass);

        const slider = content.querySelector(".swiper-container");
        if (slider) {
            slider.dispatchEvent(new CustomEvent("updateSlider"));
        }
    }

    closeContent(content) {
        content.classList.remove(this.activeClass);
    }
}

const simpleTab = new TabGroup({
    group: "[data-tab-group]",
});

export { simpleTab };
